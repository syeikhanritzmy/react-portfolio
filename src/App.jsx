import Footer from './components/Footer/Footer';
import Navbar from './components/Navbar/Navbar';
import PublicRoute from './routes/PublicRoute';

function App() {
  return (
    <div className="App">
      {' '}
      <Navbar />
      <PublicRoute />
      <Footer />
    </div>
  );
}

export default App;
