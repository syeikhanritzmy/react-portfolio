import React from 'react';

export default function Footer() {
  return (
    <div className=" w-full text-center h-20 bg-black text-white flex items-center justify-center mt-14">
      <div className="container mx-auto  ">
        {' '}
        Copyright ©️ 2022 ~ RCTN-KS06-010
      </div>
    </div>
  );
}
