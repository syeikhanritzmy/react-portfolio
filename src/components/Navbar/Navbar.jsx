import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { SiWebpack } from 'react-icons/si';
export default function Navbar() {
  const [navbar, setNavbar] = useState(false);
  let activeClassName = 'font-bold text-blue-300  ease-out duration-300 ';
  return (
    <nav className="w-full bg-white drop-shadow-lg px-5 absolute">
      <div className="justify-between px-4-mx-auto lg:max-w-7 md:items-center md:flex md:px-8">
        <div>
          <div className="flex items-center justify-between py-3 md:py-5 md:block sm:pl-5 md:pl-0">
            <NavLink className="text-2xl font-bold" to={'/'}>
              <SiWebpack size={'35px'} />
            </NavLink>
            <div className="md:hidden">
              <button
                className="p-2 text-gray-700 rounded-md outline-none focus:border-gray-400 focus:border"
                onClick={() => {
                  setNavbar(!navbar);
                }}
              >
                {navbar ? (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-6 h-6 text-black"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                ) : (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-6 h-6 text-black"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M4 6h16M4 12h16M4 18h16"
                    />
                  </svg>
                )}
              </button>
            </div>
          </div>
        </div>

        <div className="w-full">
          <div
            className={`flex  justify-self-center pb-3    mt-8 md:block md:pb-0 sm:pl-5 md:mt-0 ${
              navbar ? 'block ' : 'hidden'
            }`}
          >
            <ul className="items-center justify-center   space-y-8 md:flex md:space-x-6 md:space-y-0">
              <li>
                <NavLink
                  to={'/'}
                  className={({ isActive }) =>
                    isActive
                      ? activeClassName
                      : 'hover:text-blue-300 ease-in-out duration-300'
                  }
                  end
                >
                  Home
                </NavLink>
              </li>
              <li>
                <NavLink
                  to={'about'}
                  className={({ isActive }) =>
                    isActive
                      ? activeClassName
                      : 'hover:text-blue-300 ease-in-out duration-300'
                  }
                >
                  About
                </NavLink>
              </li>
              <li>
                <NavLink
                  to={'experience'}
                  className={({ isActive }) =>
                    isActive
                      ? activeClassName
                      : 'hover:text-blue-300 ease-in-out duration-300'
                  }
                >
                  Experience
                </NavLink>
              </li>
              <li>
                <NavLink
                  to={'skills'}
                  className={({ isActive }) =>
                    isActive
                      ? activeClassName
                      : 'hover:text-blue-300 ease-in-out duration-300'
                  }
                >
                  Skills
                </NavLink>
              </li>
              <li>
                <NavLink
                  to={'interest'}
                  className={({ isActive }) =>
                    isActive
                      ? activeClassName
                      : 'hover:text-blue-300 ease-in-out duration-300'
                  }
                >
                  Interest
                </NavLink>
              </li>
              <li>
                <NavLink
                  to={'awards'}
                  className={({ isActive }) =>
                    isActive
                      ? activeClassName
                      : 'hover:text-blue-300 ease-in-out duration-300'
                  }
                >
                  Awards
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
}
