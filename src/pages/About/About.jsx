import React from 'react';

export default function About() {
  return (
    <div className="container mx-auto   w-full">
      <div className="grid px-4 grid-cols-1 grid-rows-1 lg:grid-rows-2 lg:grid-cols-2 pt-40  items-center justify-items-center  ">
        <div className="     border-4 border-black p-4">
          <img src="https://static.zerochan.net/Ayanokouji.Kiyotaka.full.1876986.jpg" />
        </div>
        <div className=" w-full flex gap-y-4 flex-col h-full py-4 justify-start lg:mb-4">
          <h1 className="text-4xl font-bold">Ayanokouji Kiyotaka </h1>
          <h2 className="lg:text-3xl text-xl font-semibold">S01T004651</h2>
          <h3 className="lg:text-2xl text-xl font-semibold">1-D</h3>
          <h4 className="lg:text-xl text-xl font-semibold">Pria</h4>
          <div className="border-b-4 lg:w-4/6 border-black"></div>
        </div>
        <div className="lg:col-span-2 lg:px-24  h-5/6 w-full ">
          <div className="lg:text-4xl text-2xl font-bold pb-3">
            Short Biography
          </div>
          <div className="border-b-4 lg:w-2/6 border-black"></div>
          <p className="text-justify pt-4">
            Almost everything about his life before the entrance exam is
            unknown. However, during a flashback, Kiyotaka is shown to be part
            of a group of children involved with an unknown organization called
            the White Room, led by his father. He seems to have maintained his
            steadfast expression even as a child, as he showed no emotion and
            watched as another child, located to his left, was having difficulty
            breathing and probably fainted for some unknown reason[1] There was
            also a time when his father approached and said that they those who
            do not reveal their talents are fools.[2] Since arriving there,
            Kiyotaka undergoes various training regiments, possibly including
            martial arts, while undergoing strenuous and challenging written
            exams, giving him extreme physical and mental abilities. As time
            went on, more children began to suffer like the previous children,
            eventually leaving him as the sole survivor of the group. His
            training made him believe that all humans are just tools and that
            victory is all that matters in this world. With this point of view,
            he is determined to win no matter what and sacrifices anything or
            anyone to achieve his goal of victory
          </p>
        </div>
      </div>
    </div>
  );
}
