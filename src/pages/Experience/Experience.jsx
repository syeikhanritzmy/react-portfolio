import React from 'react';

export default function Experience() {
  return (
    <div className="container mx-auto w-full h-screen pt-24">
      <div className="grid grid-cols-1 lg:grid-cols-2 justify-items-center h-3/6 items-center">
        <div className="col-span-3 text-center">
          <h1 className="font-bold text-4xl space-x-4 pb-2">
            years of agency Experience
          </h1>
          <h2 className="italic text-center">
            Sample text, click to select the text box. click again or double
            click to start editing the text
          </h2>
        </div>
        <div className="col-span-3 w-6/12 pt-2  h-full">
          <div className="grid grid-cols-1 lg:grid-cols-3 md:grid-cols-1 w-full h-full justify-items-center   gap-x-5 gap-y-5">
            <div className="bg-blue-300  w-full flex justify-center items-center font-bold lg:text-xl uppercase  p-5">
              Smk 3{' '}
            </div>
            <div className="bg-red-300  w-full flex justify-center items-center font-bold lg:text-xl uppercase text-center  p-5">
              {' '}
              <p> study in information systems</p>
            </div>
            <div className=" bg-yellow-300  w-full flex justify-center items-center font-bold lg:text-xl uppercase text-center p-5 ">
              <p>had an internship</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
