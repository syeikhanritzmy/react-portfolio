import React from 'react';
import { BsGithub, BsInstagram, BsLinkedin } from 'react-icons/bs';
export default function Home() {
  return (
    <div className="container mx-auto  h-full     mt-4 ">
      <div className="grid grid-cols-1  md:grid-cols-2 lg:grid-cols-2 gap-4 lg:gap-8 h-screen ">
        <div className="flex justify-center p-6 items-center flex-col  ">
          <div className="">
            <h1 className="font-bold lg:text-4xl text-3xl   sm:text-center">
              Syeikhan Ritzmy 🛩️
            </h1>
            <div className="border-b-4 border-black lg:w-4/6 mt-2  sm:w-full "></div>
            <h2 className="mt-2 text-center text-lg lg:text-left lg:text-xl">
              Frontend Developer
            </h2>
            <div className="flex lg:w-40 justify-around lg:justify-start lg:gap-x-3 mt-2 ">
              <a href="https://github.com/syeikhanritzmy" target={'_blank'}>
                <BsGithub className="lg:text-xl text-3xl" />
              </a>
              <a
                href="https://linkedin.com/in/syeikhan-ritzmy-11071b199/"
                target={'_blank'}
              >
                <BsLinkedin className="lg:text-xl  text-3xl" />
              </a>
              <a href="https://instagram.com/syeikhaan" target={'_blank'}>
                <BsInstagram className="lg:text-xl  text-3xl" />
              </a>
            </div>
          </div>
        </div>
        <div className="flex justify-center  p-6  items-center text-justify text-base lg:text-lg ">
          {' '}
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit, facere
          labore ratione odit fugit earum corporis voluptatem exercitationem,
          animi rerum velit, possimus aliquid mollitia saepe deserunt commodi
          iste quia sed. Doloremque quos rem, cupiditate a itaque est
          praesentium modi doloribus, accusamus nemo reiciendis reprehenderit.
          Laudantium impedit inventore quasi nihil aliquid est, quam consequatur
          non velit exercitationem. Possimus minima aut iste consequatur ullam!
          Corrupti, facilis. veritatis!
        </div>
      </div>
    </div>
  );
}
