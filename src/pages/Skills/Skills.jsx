import React from 'react';

export default function Skills() {
  return (
    <div className="pt-20 container mx-auto  ">
      <div className=" w-full p-20  grid  grid-cols-1 md:grid-cols-1 lg:grid-cols-3 items-center justify-items-center gap-y-5  h-screen ">
        <div className=" w-60 bg-lime-300  h-full lg:h-3/6 text-center flex justify-center items-center font-medium text-xl drop-shadow-md rounded-lg">
          Web Development
        </div>
        <div className=" w-60 bg-purple-300  h-full lg:h-3/6 text-center flex justify-center items-center font-medium text-xl drop-shadow-md rounded-lg">
          Mobile
        </div>
        <div className=" w-60 bg-orange-300  h-full lg:h-3/6 text-center flex justify-center items-center font-medium text-xl drop-shadow-md rounded-lg">
          Ilustrator
        </div>
      </div>
    </div>
  );
}
